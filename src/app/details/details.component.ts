import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { WebServiceService } from '../service/web-service.service';
import { Post } from 'src/Models/Post';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnChanges {

  @Input() id: number;

  @Input() clickInfo: boolean;

  post: any = {};

  constructor(private webService: WebServiceService) { }

  ngOnInit(): void {
    this.getPost(this.id);
  }

  ngOnChanges(change){
    console.log(this.clickInfo);
    this.getPost(this.id);
  }

  getPost(id) {
    this.webService.getPost(id).then(response => {console.log(this.post = response); });
  }

}
