import { Component, OnInit } from '@angular/core';
import { WebServiceService } from './service/web-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ProjectB';
  id = 1;
  clickInfo = false;

  constructor(private webService: WebServiceService){}

 ngOnInit() {
  }
  
  sendPostId(id) {
    console.log(id);
    this.id = id;
  }

  sendClickInfo(clickInfo){
    this.clickInfo = clickInfo;
  }
}
