import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WebServiceService } from '../service/web-service.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  posts = [];

  postsTemp: any = [];

  filter: string;

  @Output() sendRequestToFather = new EventEmitter();

  @Output() sendClickInfoToFather = new EventEmitter();

  constructor(private webService: WebServiceService) { }

  ngOnInit(): void {
    //this.getAllPost(true);
  }
  getAllPost(clickEmitToFather) {
    this.webService.getAllPost().then(response => {this.posts = response; });
    if (clickEmitToFather === true) {
      this.sendClickInfoToFather.emit(true);
    }
  }

  sendPostId(id: any) {
    this.sendRequestToFather.emit(id);
  }

  filterPost(){

    if (this.filter === '') {
      this.getAllPost(false);
    } else {
        this.postsTemp = this.posts;
        this.posts = [];

        this.postsTemp.forEach(element => {
          if (element.title.startsWith(this.filter)) {
            this.posts.push(element);
          }

        });
    }


  }

}
