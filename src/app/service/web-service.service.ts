import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebServiceService {

  posts = [];
  post = {};

  constructor(private httpClient: HttpClient) { }

  async getAllPost(){
    await this.httpClient
     .get<any[]>('https://jsonplaceholder.typicode.com/posts').toPromise().then(
       response => {
         this.posts = response;
       }
     );
     console.log(this.posts);
    return this.posts;
  }

  async getPost(id){
    await this.httpClient
     .get('https://jsonplaceholder.typicode.com/posts/' + id).toPromise().then(
       response => {
         this.post = response;
       }
     );
    return this.post;
  }
}
